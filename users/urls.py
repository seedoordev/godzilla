from django.contrib.auth.decorators import login_required
from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', home_view, name='home'),
    path('training-calendar/', CalendarListView.as_view(), name='calendar'),
    path('training-calendar/<int:training_id>/', training_view, name='training'),
    path('training-calendar/<training_id>/delete/', TrainingDeleteView.as_view(), name='delete_training'),
    path('training-calendar/<training_id>/update/', TrainingUpdateView.as_view(), name='update_training'),
    path('training-calendar/create', TrainingCreateView.as_view(), name='create_training'),
    path('profile/', profile_view, name='profile'),
    path('articles/', ArticleListView.as_view(), name='articles'),
    path('articles/<int:pk>', ArticleDetailView.as_view(), name='articles_detail'),
    path('chat/', chat_view, name='chat'),
    path('chat_get_messages/<int:chat_id>/', get_messages_view, name='chat_get_messages'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
