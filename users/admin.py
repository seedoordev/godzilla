from django.contrib import admin
from django.utils.safestring import mark_safe
from modeltranslation.admin import TranslationAdmin

from .models import *


@admin.register(WorkoutАrticle)
class WorkoutАrticleAdmin(TranslationAdmin):
    list_display = ('title', 'get_html_photo', 'date', 'content', 'allowed_by_admin')
    search_fields = ('title',)
    list_editable = ('allowed_by_admin',)
    list_filter = ('date',)
    fields = ('title', 'preview_image', 'get_html_photo', 'date', 'content', 'allowed_by_admin')
    readonly_fields = ('get_html_photo',)
    save_on_top = True

    def get_html_photo(self, object):
        if object.preview_image:
            return mark_safe(f"<img src='{object.preview_image.url}' width=50>")


@admin.register(Gym)
class GymAdmin(TranslationAdmin):
    list_display = ('address',)
    search_fields = ('address',)


admin.site.register(CustomUser)
admin.site.register(Training)
admin.site.register(ClientTrainer)
admin.site.register(UserResult)
admin.site.register(BookingSeasonTickets)
admin.site.register(ChatMessage)

admin.site.site_title = "Godzilla Admin"
admin.site.site_header = "Godzilla Admin"
