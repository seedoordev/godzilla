from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone as tz
from .managers import CustomUserManager
from django.core.validators import RegexValidator


class IntegerRangeField(models.IntegerField):
    """Кастомное поле модели для установления минимального и максимального значения числа"""

    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


class CustomUser(AbstractUser):
    username = None
    email = models.EmailField('Email', unique=True)
    is_trainer = models.BooleanField(_('Тренер'), default=False)
    gym = models.ForeignKey('Gym', on_delete=models.CASCADE, verbose_name=_('Зал'), null=True, blank=True)
    image = models.ImageField(_('Аватарка'), null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url

    def __str__(self):
        return self.get_full_name()

    class Meta:
        verbose_name = _("Пользователь")
        verbose_name_plural = _("Пользователи")


class ClientTrainer(models.Model):
    client_id = models.OneToOneField('CustomUser', on_delete=models.CASCADE, related_name='client', unique=True,
                                     null=True, blank=True, verbose_name=_('Клиент'))
    trainer_id = models.ForeignKey('CustomUser', on_delete=models.CASCADE, related_name='trainer', null=True,
                                   blank=True, verbose_name=_('Тренер'))

    def __str__(self):
        return self.client_id.get_full_name()

    class Meta:
        verbose_name = _('Пара клиент-тренер')
        verbose_name_plural = _('Пары клиент-тренер')


class ChatMessage(models.Model):
    chat = models.ForeignKey(ClientTrainer, on_delete=models.CASCADE, verbose_name=_("Чат"))
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name=_("Пользователь"))
    message = models.TextField(_("Сообщение"))
    pub_date = models.DateTimeField(_('Дата сообщения'), default=tz.now)

    class Meta:
        ordering = ['pub_date']
        verbose_name = _("Сообщение в чате")
        verbose_name_plural = _("Сообщения в чате")

    def __str__(self):
        return self.message


class Training(models.Model):
    user = models.ForeignKey('CustomUser', on_delete=models.CASCADE, related_name='trainings', unique_for_date='date',
                             verbose_name=_('Пользователь'))
    title = models.CharField(max_length=40, verbose_name=_('Заголовок'))
    content = models.TextField(null=True, blank=True, max_length=1000, verbose_name=_('Контент'))
    date = models.DateField(verbose_name='Дата')
    is_completed = models.BooleanField(verbose_name=_('Тренировка завершена'))
    is_missed = models.BooleanField(verbose_name=_('Тренировка пропущена'))
    comment = models.CharField(null=True, blank=True, max_length=500, verbose_name=_('Комментарий'))

    # client_trainer_id = models.ForeignKey('ClientTrainer', on_delete=models.CASCADE, related_name='trainer')

    def get_absolute_url(self):
        return reverse('training', kwargs={"training_id": self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Тренировка")
        verbose_name_plural = _("Тренировки")
        constraints = [
            models.UniqueConstraint(fields=['user', 'date'], name='Unique')
        ]


class Gym(models.Model):
    address = models.CharField(max_length=150, verbose_name=_('Зал'))

    def __str__(self):
        return self.address

    class Meta:
        verbose_name = _("Тренажёрный зал")
        verbose_name_plural = _("Тренажёрные залы")


class UserResult(models.Model):
    user = models.OneToOneField('CustomUser', on_delete=models.CASCADE, unique=True, verbose_name=_('Пользователь'))
    deadlift = IntegerRangeField(null=True, blank=True, min_value=0, max_value=600, verbose_name=_('Становая'))
    squats = IntegerRangeField(null=True, blank=True, min_value=0, max_value=600, verbose_name=_('Приседания'))
    benchpress = IntegerRangeField(null=True, blank=True, min_value=0, max_value=600, verbose_name=_('Жим'))

    def __str__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = _("Результаты пользователя")
        verbose_name_plural = _("Результаты пользователей")


class BookingSeasonTickets(models.Model):
    phone_regex = RegexValidator(
        regex=r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
        message=_("Введите правильный номер телефона"))
    phone_number = models.CharField(validators=[phone_regex], max_length=17, verbose_name=_('Номер клиента'))

    def __str__(self):
        return self.phone_number

    class Meta:
        verbose_name = _("Номер для бронирования")
        verbose_name_plural = _("Номера для бронирования")


class WorkoutАrticle(models.Model):
    title = models.CharField(_('Заголовок'), max_length=40)
    preview_image = models.ImageField(_("Картинка"))
    date = models.DateField(_('Дата'))
    content = models.TextField(_('Контент'), max_length=50000)
    allowed_by_admin = models.BooleanField(_("Одобрено администратором к публикации"), default=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Спортиная статья")
        verbose_name_plural = _("Спортивные статьи")
