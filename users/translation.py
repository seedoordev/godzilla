from modeltranslation.translator import register, TranslationOptions
from .models import WorkoutАrticle, Gym


@register(WorkoutАrticle)
class WorkoutАrticleTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


@register(Gym)
class GymTranslationOptions(TranslationOptions):
    fields = ('address',)
