from allauth.account.forms import SignupForm
from django import forms
from .models import Training, CustomUser, ClientTrainer, UserResult, BookingSeasonTickets, ChatMessage


class CustomUserSignupForm(SignupForm):
    first_name = forms.CharField(max_length=30, label='Ваше имя', required=True)
    last_name = forms.CharField(max_length=30, label='Ваша фамилия', required=True)

    def save(self, request):
        user = super(CustomUserSignupForm, self).save(request)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()
        return user


class TrainingCreateForm(forms.ModelForm):
    class Meta:
        model = Training
        fields = ['date', 'title', 'content', 'is_completed', 'is_missed', 'comment']


class TrainingUpdateForm(forms.ModelForm):
    class Meta:
        model = Training
        fields = ['date', 'title', 'content', 'is_completed', 'is_missed', 'comment']


class UserGymForm(forms.ModelForm):
    prefix = 'gym'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['gym'].widget.attrs.update({'onchange': 'submit();'})

    class Meta:
        model = CustomUser
        fields = ['gym']


class UserImageForm(forms.ModelForm):
    prefix = 'image'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'onchange': 'submit();'})

    class Meta:
        model = CustomUser
        fields = ['image']


class UserChangeTrainerForm(forms.ModelForm):
    prefix = 'changetrainer'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['trainer_id'].widget.attrs.update({'onchange': 'submit();'})

    class Meta:
        model = ClientTrainer
        fields = ['trainer_id']


class UserChangeResultsForm(forms.ModelForm):
    prefix = 'changeresults'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['deadlift'].widget.attrs.update({'onchange': 'submit();'})
        self.fields['squats'].widget.attrs.update({'onchange': 'submit();'})
        self.fields['benchpress'].widget.attrs.update({'onchange': 'submit();'})

    class Meta:
        model = UserResult
        fields = ['deadlift', 'squats', 'benchpress']


class BookingSeasonTicketsForm(forms.ModelForm):
    class Meta:
        model = BookingSeasonTickets
        fields = ['phone_number']


class ChatMessageForm(forms.ModelForm):
    class Meta:
        model = ChatMessage
        fields = ['message']
