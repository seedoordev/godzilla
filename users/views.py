from datetime import timedelta, datetime
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from django.db.models import Q

from .forms import TrainingCreateForm, TrainingUpdateForm, UserGymForm, UserImageForm, UserChangeTrainerForm, \
    UserChangeResultsForm, BookingSeasonTicketsForm, ChatMessageForm
from .models import *


def home_view(request):
    """Логика домашней страницы с бронированием абонемента"""
    if request.method == 'POST':
        bookingform = BookingSeasonTicketsForm(request.POST)
        if bookingform.is_valid():
            bookingform.save()
    else:
        bookingform = BookingSeasonTicketsForm()
    return render(request, 'users/home.html', {'bookingform': bookingform})


class CalendarListView(ListView):
    model = Training

    '''В queryset передаю тренировочные дни, которые входят в необходимый диапазон'''

    def get_queryset(self):
        seven_days = timedelta(7)

        return Training.objects.filter(Q(user_id=self.request.user.id) &
                                       Q(date__gte=tz.localtime(tz.now()).date() - seven_days) &
                                       Q(date__lte=tz.localtime(tz.now()).date() + seven_days)).order_by('date')

    '''В контексте передаю все 15 дней, которые должны быть отражены, тренировочные дни и сегодняшний день'''
    '''Полностью осознаю, что данное решение - костыль с кучей логики в шаблоне'''

    def get_context_data(self, **kwargs):
        context = super(CalendarListView, self).get_context_data(**kwargs)
        start_date = tz.localtime(tz.now()).date() - timedelta(7)
        training_days = list(Training.objects.filter(user_id=self.request.user.id).values('date'))
        context['training_days_clean'] = [day['date'] for day in training_days]
        context['days'] = [start_date + timedelta(day) for day in range(15)]
        context['today'] = tz.localtime(tz.now()).date()
        return context


def training_view(request, training_id):
    if not request.user.is_authenticated:
        return redirect('account_login')
    training = get_object_or_404(Training,
                                 Q(user=request.user) &
                                 Q(pk=training_id))
    context = {
        'training': training,
    }
    return render(request, 'users/training_detail.html', context)


class TrainingCreateView(CreateView):
    model = Training
    form_class = TrainingCreateForm
    template_name = 'users/training_create.html'

    def form_valid(self, form):
        form.instance.user_id = self.request.user.id
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TrainingCreateView, self).get_context_data(**kwargs)
        context['date'] = datetime.strptime(self.request.GET.get('date', ''), '%Y-%m-%d').date()
        return context


class TrainingUpdateView(UpdateView):
    model = Training
    pk_url_kwarg = 'training_id'
    template_name = 'users/training_update.html'
    form_class = TrainingUpdateForm


class TrainingDeleteView(DeleteView):
    model = Training
    success_url = reverse_lazy('calendar')
    pk_url_kwarg = 'training_id'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


def profile_view(request):
    if not request.user.is_authenticated:
        return redirect('account_login')
    """ Функция отображает и меняет выбранного тренера, картинку профиля, тренажёрный зал, результаты пользователя"""

    # Попытка получить связь тренера и пользователя в которой учавствует текущий пользовател
    try:
        client_trainer = ClientTrainer.objects.get(client_id=request.user.id)

    # Если такая связь не найдена она создаётся, в качестве клиента передаётся текущий пользователь
    except:
        client_trainer = ClientTrainer(client_id=request.user)
        client_trainer.save()
        client_trainer = ClientTrainer.objects.get(client_id=request.user)

    # Если пользоваетель смотрит или меняет картинку
    if request.method == 'POST' and ('image-image' in request.POST):
        imageform = UserImageForm(request.POST, request.FILES, prefix='image', instance=request.user)
        if imageform.is_valid():
            imageform.save()
    else:
        imageform = UserImageForm(prefix='image')

    # Если пользоваетель смотрит или меняет тренажёрный зал
    if request.method == 'POST' and ('gym-gym' in request.POST):
        gymform = UserGymForm(request.POST, prefix='gym', instance=request.user)
        if gymform.is_valid():
            gymform.save()
    else:
        gymform = UserGymForm(prefix='imagegym')

    # Если пользоваетель смотрит или меняет выбранного тренера
    if request.method == 'POST' and ('changetrainer-trainer_id' in request.POST):
        changetrainerform = UserChangeTrainerForm(request.POST, instance=client_trainer, prefix='changetrainer')
        if changetrainerform.is_valid():
            changetrainerform.save()
    else:
        changetrainerform = UserChangeTrainerForm(prefix='changetrainer')

    # Попытка получить результаты прользователя
    try:
        user_results_instance = UserResult.objects.get(user=request.user.id)
    # Если результаты пользователя не найдены создаётся новая таблица с результатами
    except UserResult.DoesNotExist:
        user_results_instance = UserResult(user=request.user)
        user_results_instance = user_results_instance.save()

    # Если пользоваетель смотрит или меняет свои результаты
    if request.method == 'POST' and (
            'changeresults-deadlift' in request.POST or 'changeresults-squats' in request.POST or 'changeresults-benchpress' in request.POST):
        changeresultsform = UserChangeResultsForm(request.POST, instance=user_results_instance, prefix='changeresults')
        if changeresultsform.is_valid():
            changeresultsform.save()
        else:
            return redirect('profile')
    else:
        changeresultsform = UserChangeResultsForm(prefix='changeresults')

    results = UserResult.objects.get(user=request.user)

    choices_gym = [(e.id, e.address) for e in Gym.objects.all()]

    choices_trainer = [(t.id, t.get_full_name()) for t in CustomUser.objects.filter(is_trainer=True)]

    context = {
        'gymform': gymform,
        'imageform': imageform,
        'changetrainerform': changetrainerform,
        'changeresultsform': changeresultsform,
        'choices_gym': choices_gym,
        'choices_trainer': choices_trainer,
        'client_trainer': client_trainer,
        'results': results,
    }

    return render(request, 'users/profile.html', context)


class ArticleListView(ListView):
    model = WorkoutАrticle
    template_name = 'users/article_list.html'

    def get_queryset(self):
        return WorkoutАrticle.objects.all().order_by('-date')


class ArticleDetailView(DetailView):
    model = WorkoutАrticle
    pk_url_kwarg = 'pk'
    template_name = 'users/article_detail.html'


def chat_view(request):
    if not request.user.is_authenticated:
        return redirect('account_login')
    # Если пользователь - тренер
    if request.user.is_trainer:
        chats = ClientTrainer.objects.filter(trainer_id=request.user)

        chat_id = request.GET.get('chat', '')
        if chat_id:
            try:
                messages = ChatMessage.objects.filter(chat_id=request.GET.get('chat', '')).order_by('-pub_date')
            except messages.DoesNotExist:
                messages = None
            current_chat = ClientTrainer.objects.get(id=request.GET.get('chat', ''))
        else:
            messages = None
            current_chat = None

        context = {
            'messages': messages,
            'chats': chats,
            'current_chat': current_chat
        }
    # Если пользователь - клиент
    else:
        try:
            chat = ClientTrainer.objects.get(client_id=request.user)
        except:
            return redirect('profile')
        try:
            messages = ChatMessage.objects.filter(chat__client_id=request.user.id).order_by('-pub_date')
        except:
            messages = None

        context = {
            'chat': chat,
            'messages': messages,
        }

    # Форма
    if request.method == 'POST':
        form = ChatMessageForm(data=request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            if request.user.is_trainer:
                if not request.GET.get('chat', ''):
                    return redirect('chat')
                message.chat_id = request.GET.get('chat', '')
            else:
                message.chat_id = ClientTrainer.objects.get(client_id=request.user.id).id
            message.author = request.user
            message.save()
            context.update({'form': message})
    else:
        form = ChatMessageForm()
        context.update({'form': form})

    return render(request, 'users/chat.html', context)


def get_messages_view(request, chat_id):
    """ Получаю все сообщения в чате где есть текущий пользователь
    [
       {
          "model":"users.chatmessage",
          "pk":1,
          "fields":{
             "chat":44,
             "author":1,
             "message":"Привет",
             "pub_date":"2021-06-13T16:03:28Z",
          }
       }
    ]
    """
    if request.user.is_trainer:
        messages = ChatMessage.objects.filter(chat_id=chat_id).order_by('-pub_date')
    else:
        messages = ChatMessage.objects.filter(chat__client_id=request.user.id).order_by('-pub_date')

    messages_json = serializers.serialize('json', messages)
    return HttpResponse(messages_json, content_type='application/json')
